﻿using CustomerApp.Core.DomainServices;
using CustomerApp.Core.Entity;
using CustomerApp.Core.Test.FakeContext;
using CustomerApp.Infra;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerApp.Core.Test.FakeRepository
{
   public class FakeCourseRepo : FakeDBRepository<Course>, ICourseRepository
    {
        public FakeCourseRepo(FakeCustomerDbContext customerDbContext) : base(customerDbContext)
        {
            //_customerDbContext = customerDbContext;
        }
    }
}
