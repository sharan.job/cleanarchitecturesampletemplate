﻿using CustomerApp.Core.DomainServices;
using CustomerApp.Core.Entity;
using System;
using System.Collections.Generic;
using CustomerApp;
using System.Text;

namespace CustomerApp.Core.ApplicaitonServices.Services
{
  public  class CourseService : ICourseServices
    {
        readonly ICourseRepository _CourseRepo;
        private IUnitOfWork _unitOfWork;
        public CourseService(ICourseRepository courseRepository, IUnitOfWork unitOfWork)
        {
            _CourseRepo = courseRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Course> GetAllCourses()
        {
            // return  _CourseRepo.GetAll();
            return _unitOfWork.Courses.GetAll();
        }

        public Course CreateCourse(Course cust)
        {
             _unitOfWork.Courses.Add(cust);

            Course course = new Course();
            return course;
        }
    }
}
