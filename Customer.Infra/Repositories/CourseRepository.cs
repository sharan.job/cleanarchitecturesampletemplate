﻿using CustomerApp.Core.Entity;
using CustomerApp.Core.DomainServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerApp.Infra.Repositories
{
   public class CourseRepository : Repository<Course>,ICourseRepository
    {
        //private readonly CustomerDbContext _customerDbContext;
        public CourseRepository(CustomerDbContext customerDbContext) : base(customerDbContext)
        {
            //_customerDbContext = customerDbContext;
        }
    }
}
