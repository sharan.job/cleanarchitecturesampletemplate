﻿using CustomerApp.Core.Entity;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerApp.Core.ApplicaitonServices.Services
{
   public class OrderValidator : AbstractValidator<Order>
    {
        public OrderValidator()
        {
            RuleFor(m => m.OrderName).NotNull().MinimumLength(3).MaximumLength(10);
        }
    }
}
