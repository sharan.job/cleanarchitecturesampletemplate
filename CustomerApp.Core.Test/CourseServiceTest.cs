using CustomerApp.Core.ApplicaitonServices.Services;
using CustomerApp.Core.DomainServices;
using CustomerApp.Core.Entity;
using CustomerApp.Core.Test.FakeRepository;
using CustomerApp.Core.Test.FakeUnity;
using CustomerApp.Infra.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace CustomerApp.Core.Test
{
    [TestClass]
    public class CourseServiceTest
    {
        private static IServiceCollection services;
        ICourseRepository _repository;
        IUnitOfWork _unitOfWork;
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {

            // services.AddScoped<IUnitOfWork, FakeUnityOfWork>();
        }



        public CourseService create()
        {
            var agent = new CourseService(_repository,new FakeUnityOfWork());

            return agent;
        }


     
        [TestMethod]
        public void getCourseDetails()
        {
            var agent = create();
            var result = agent.GetAllCourses();
          //  var result=  _unitOfWork.Courses.GetAll();
            Assert.IsNull(result);

        }

        }
}
