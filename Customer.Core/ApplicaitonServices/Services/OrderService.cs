﻿using CustomerApp.Core.DomainServices;
using CustomerApp.Core.Entity;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CustomerApp.Core.ApplicaitonServices.Services
{
    public class OrderService : IOrderService
    {
        readonly IOrderRepository _orderRepo;
        readonly ICustomerRepository _customerRepo;

        public OrderService(IOrderRepository orderRepo
           )
        {
            _orderRepo = orderRepo;
          
        }

        public Order New()
        {
            return new Order();
        }

        public Order CreateOrder(Order order)
        {
            //if (order.Customer == null || order.Customer.Id <= 0)
            //    throw new InvalidDataException("To create Order you need a Customer");
            //if (_customerRepo.ReadyById(order.Customer.Id) == null)
            //    throw new InvalidDataException("Customer Not found");
            //if (order.OrderDate == null)
            //    throw new InvalidDataException("Order needs a Order Date");

            return _orderRepo.Create(order);
        }

        public Order FindOrderById(int id)
        {
            return _orderRepo.ReadyById(id);
        }

        public List<Order> GetAllOrders()
        {
            return _orderRepo.ReadAll().ToList();
        }

        public Order UpdateOrder(Order orderUpdate)
        {
            return _orderRepo.Update(orderUpdate);
        }

        public Order DeleteOrder(int id)
        {
            return _orderRepo.Delete(id);
        }
    }
}
