﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CustomerApp.Core.Entity;
using CustomerApp.Core.ApplicaitonServices;
using CustomerApp.Core.DomainServices;

namespace CleanArchitectureSample.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {

        private readonly ICourseServices _courseServices;

        public CourseController(ICourseServices courseServices)
        {
            _courseServices = courseServices;
        }


        public IEnumerable<Course> ReadAll()
        {
            return _courseServices.GetAllCourses();
        }


        [HttpPost]
        public ActionResult<Course> Post([FromBody] Course course)
        {
            return _courseServices.CreateCourse(course);
        }
    }
}