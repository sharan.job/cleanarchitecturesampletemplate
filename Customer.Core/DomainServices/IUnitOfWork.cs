﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerApp.Core.DomainServices
{
    public interface IUnitOfWork : IDisposable
    {
        ICourseRepository Courses { get; }

        int Complete();
    }
}
