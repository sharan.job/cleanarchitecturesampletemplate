﻿using CustomerApp.Core.DomainServices;
using System;
using System.Collections.Generic;
using System.Text;
using CustomerApp.Core.Entity;

namespace CustomerApp.Infra.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly CustomerDbContext _customerDbContext;

        public CustomerRepository(CustomerDbContext customerDbContext) 
        {
            _customerDbContext = customerDbContext;
        }


        public Customer Create(Customer customer) 
        {
            try
            { 
          var result=  _customerDbContext.Customers.Add(customer);
                _customerDbContext.SaveChanges();
            return customer;
            }
            catch(Exception ex)
            {
                return null;
            }

        }

        public Customer Delete(int id)
        {
            return null;
        }

        public IEnumerable<Customer> ReadAll()
        {
            return _customerDbContext.Customers;
        }

        public Customer ReadyById(int id)
        {
            return _customerDbContext.Customers.Find(id);
        }

        public Customer Update(Customer customerUpdate)
        {
            return null;
        }
    }
}
