﻿using CustomerApp.Core.DomainServices;
using CustomerApp.Core.Test.FakeContext;
using CustomerApp.Infra;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace CustomerApp.Core.Test.FakeRepository
{
   public class FakeDBRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {

        protected readonly FakeCustomerDbContext Context;
        public FakeDBRepository(FakeCustomerDbContext context)
        {
            Context = context;
        }


        public void Add(TEntity entity)
        {
           
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
          
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return null;
        }

        public TEntity Get(int id)
        {
            return null;
        }

        public IEnumerable<TEntity> GetAll()
        {
            return null;
        }

        public void Remove(TEntity entity)
        {
            
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            
        }
    }
}
