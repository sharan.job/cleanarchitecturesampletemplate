﻿using CustomerApp.Core.Entity;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerApp.Core.ApplicaitonServices.Services
{
   public class CustomerValidator: AbstractValidator<Customer>
    {
        // private readonly IServiceProvider serviceProvider;
        //public CustomerValidator(IServiceProvider serviceProvider)
        public CustomerValidator()
        {
            //this.serviceProvider = serviceProvider;
            RuleFor(m => m.FirstName).NotEmpty().MinimumLength(3).MaximumLength(100);
        }
    }
}
