﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerApp.Core.ApplicaitonServices;
using CustomerApp.Core.ApplicaitonServices.Services;
using CustomerApp.Core.DomainServices;
using CustomerApp.Infra;
using CustomerApp.Infra.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle;
using Swashbuckle.AspNetCore.Swagger;
using FluentValidation;
using FluentValidation.AspNetCore;
using CustomerApp.Core.Entity;
using Serilog;

namespace CleanArchitectureSample
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(opt =>
            {
             //   opt.Filters.Add(typeof(ValidatorActionFilter));
            }).AddFluentValidation(fvc => { });
            //.AddFluentValidation(fvc=>fvc.RegisterValidatorsFromAssemblyContaining<CustomerValidator>());
            services.AddTransient<IValidator<Customer>, CustomerValidator>();
            services.AddTransient<IValidator<Order>, OrderValidator>();

            

            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<ICustomerService, CustomerService>();

            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IOrderService, OrderService>();

            services.AddScoped<ICourseRepository, CourseRepository>();
            services.AddScoped<ICourseServices, CourseService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();


            services.AddDbContext<CustomerDbContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("CustomerDatabase")));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            }
                );

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseSwaggerUI(
                c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                });
            app.UseMvc();
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddSerilog();
            loggerFactory.AddFile("D:/Logs/mylog-{Date}.txt");
        }
    }
}
