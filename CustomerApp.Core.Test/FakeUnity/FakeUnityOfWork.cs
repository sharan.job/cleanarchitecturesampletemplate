﻿using CustomerApp.Core.DomainServices;
using CustomerApp.Core.Test.FakeContext;
using CustomerApp.Core.Test.FakeRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerApp.Core.Test.FakeUnity
{
    public class FakeUnityOfWork : IUnitOfWork
    {
       
        public ICourseRepository Courses { get; private set; }
        private readonly FakeCustomerDbContext _context;

        public FakeUnityOfWork()
        {
            _context =new FakeCustomerDbContext();
            Courses = new FakeCourseRepo(_context);
        }



        public int Complete()
        {
            return 1;
          //  throw new NotImplementedException();
        }

        public void Dispose()
        {
          //  throw new NotImplementedException();
        }
    }
}
