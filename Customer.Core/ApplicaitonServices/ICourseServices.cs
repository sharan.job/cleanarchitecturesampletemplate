﻿using CustomerApp.Core.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerApp.Core.ApplicaitonServices
{
  public  interface ICourseServices
    {
        IEnumerable<Course> GetAllCourses();

        Course CreateCourse(Course cust);
    }
}
