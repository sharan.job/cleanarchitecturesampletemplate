﻿using System;
using System.Collections.Generic;
using System.Text;
using CustomerApp.Core.DomainServices;
using CustomerApp.Infra.Repositories;

namespace CustomerApp.Infra
{
   public class UnitOfWork : IUnitOfWork
    {
        private readonly CustomerDbContext _context;
        public ICourseRepository Courses { get; private set; }



        public UnitOfWork(CustomerDbContext context)
        {
            _context = context;
            Courses = new CourseRepository(_context);
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
