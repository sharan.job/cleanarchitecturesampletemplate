﻿using System;
using System.Collections.Generic;
using System.Text;
using CustomerApp.Core.Entity;

namespace CustomerApp.Core.ApplicaitonServices
{
    public interface IOrderService
    {
        //New Order
        Order New();

        //Create //POST
        Order CreateOrder(Order order);
        //Read //GET
        Order FindOrderById(int id);
        List<Order> GetAllOrders();
        //Update //PUT
        Order UpdateOrder(Order orderUpdate);

        //Delete //DELETE
        Order DeleteOrder(int id);
    }
}
