﻿using System;
using System.Collections.Generic;
using System.Text;
using CustomerApp.Core.DomainServices;
using CustomerApp.Core.Entity;

namespace CustomerApp.Infra.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly CustomerDbContext _customerDbContext;


        public OrderRepository(CustomerDbContext customerDbContext)
        {
            _customerDbContext = customerDbContext;
        }
        public Order Create(Order order)
        {
            try
            {
                var result = _customerDbContext.Orders.Add(order);
                _customerDbContext.SaveChanges();
                return order;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public Order Delete(int id)
        {
            return null;
        }

        public IEnumerable<Order> ReadAll()
        {
            return _customerDbContext.Orders;
        }

        public Order ReadyById(int id)
        {
            return null;
        }

        public Order Update(Order OrderUpdate)
        {
            return null;
        }
    }
}
