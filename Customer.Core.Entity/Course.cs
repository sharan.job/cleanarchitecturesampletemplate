﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerApp.Core.Entity
{
   public class Course
    {
        public int CourseId { get; set; }
        public string CourseName { get; set; }
    }
}
