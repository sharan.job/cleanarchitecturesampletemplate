﻿using System;
using System.Collections.Generic;
using System.Text;
using CustomerApp.Core.Entity;

namespace CustomerApp.Core.DomainServices
{
   public interface ICustomerRepository
    {
        Customer Create(Customer customer);
        //Read Data
        Customer ReadyById(int id);
       IEnumerable<Customer> ReadAll();
        //Update Data
        Customer Update(Customer customerUpdate);
        //Delete Data
        Customer Delete(int id);
    }
}
